const body = document.getElementById('background')
let photosArray = []

function ableToGetLocation() {
    status.textContent = 'Unable to get your location';
}

if (!navigator.geolocation) {
    status.textContent = 'Geolocation is not supported by your browser';
} else {
    status.textContent = 'Locating…';
    navigator.geolocation.getCurrentPosition(getPhotos);
}

ableToGetLocation()

const img = document.createElement('img')
const img2 = document.createElement('img')
const img3 = document.createElement('img')
const img4 = document.createElement('img')
const img5 = document.createElement('img')

const box = document.getElementById('box')

function renderPhoto(data) {
    photosArray = data

    setTimeout(function() {
        img.setAttribute("src", photosArray[0]);
        box.appendChild(img)
    }, 1000)

    setTimeout(function() {
        img2.setAttribute("src", photosArray[1]);
        box.appendChild(img2)
    }, 2000)

    setTimeout(function() {
        img3.setAttribute("src", photosArray[2]);
        box.appendChild(img3)
    }, 3000)

    setTimeout(function() {
        img4.setAttribute("src", photosArray[3]);
        box.appendChild(img4)
    }, 4000)

    setTimeout(function() {
        img5.setAttribute("src", photosArray[4]);
        box.appendChild(img5)
    }, 5000)


}

function constructImageURL(photoObj) {
    let photoUrl = "https://farm" + photoObj.farm +
        ".staticflickr.com/" + photoObj.server +
        "/" + photoObj.id + "_" + photoObj.secret + ".jpg";

    return photoUrl
}

function getPhotos(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    status.textContent = '';
    fetch(`https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=936ad4c6247ee95b542afb303eac30d8&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=${latitude}&lon=${longitude}&text=parques`)
        .then(result => result.json()).then(data => renderPhoto(data.photos.photo.map(element => constructImageURL(element))))
}